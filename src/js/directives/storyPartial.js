/**
 * Created by nick on 2015-03-21.
 */
(function(){
    'use strict';

    ghostmonk.directive('storyPartial', storyPartial);

    function storyPartial() {

        var output = {
            templateUrl: 'templates/partial-story.html',
            replace: true,
            restrict: 'E',
            scope: {story: '=story'}
        };

        return output;

    }

})();
