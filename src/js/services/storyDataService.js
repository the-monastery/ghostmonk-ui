(function () {
    'use strict';

    ghostmonk.factory('storyDataService', storyDataService);
    storyDataService.$inject = ['$q', '$timeout'];

    function storyDataService($q, $timeout) {

        var deferredStoryList;
        var deferredStory;

        function getStoryList() {

            deferredStoryList = $q.defer();

            $timeout(function(){
                deferredStoryList.resolve(buildStoryList());
            }, 10);

            return deferredStoryList.promise;
        }

        function getStory(storyId) {

            deferredStory = $q.defer();

            $timeout(function(){
                deferredStory.resolve(stories[storyId]);
            }, 10);

            return deferredStory.promise;
        }


        return {
            getStoryList: getStoryList,
            getStory: getStory
        };
    }

    function buildStoryList(){
        var storyList = [];

        for( var key in stories ){
            if(stories.hasOwnProperty(key)){
                storyList.push(stories[key]);
            }
        }

        storyList.sort(function(x, y){
            return x.publishDate - y.publishDate;
        });

        storyList.reverse();

        return storyList;
    }

    var stories = {

        'frightening-portent' : {
            slug: 'frightening-portent',
            title: 'Churchill\'s Frightening Portent',
            publishDate: new Date(2015, 6, 13),

            preamble:'<p>Did you know that Winston Churchill was worried about drones well before the ' +
            'Second World War?</p><p>Well he was. Just take a look at this excerpt from "The Gathering Storm."</p>',

            complete:'<p>"Could not explosives even of the ' +
            'existing type be guided automatically in flying machines by wireless or other rays, without a human pilot, in ceaseless procession ' +
            'upon a hostile city, arsenal, camp, or dockyard?"</p>'+
            '<p>I don\'t think it is possible to understate the dangers of ignoring the lessons of World War II .</p>' +
            '<p>The unrivaled scope and destruction are probably the most important lesson we can impart to our children. ' +
            'I find it horrifying that 75+ years before the Middle East, that one of the foremost participants and intellectuals ' +
            'of the era could so astutely predict the actions of another Nation.</p>'

        },

        'up-for-air' : {
            slug: 'up-for-air',
            title: 'Coming Up For Air',
            publishDate: new Date(2015, 4, 7),

            preamble:'<p>I finally finished the build pipeline, and it took way longer than I assumed it would. ' +
            'I\'m not sure if it was my poor work ethic or my three children that put me into this functionality deficit' +
            ' but at least I can say that I\'m not running code debt.</p> ' +

            '<p>No, I have to say, this UI pipeline is beautiful. I just wish I could take all the credit.</p>',

            complete: '<p>Just look at the source code. It\'s uglified, minified and concatified, and all happens simply ' +
            'by pressing a button. For the last 2 weeks, in every spare moment that didn\'t turn into a link chasing web ' +
            'surfing fiasco, (oh, my building was a historical textile factory? Well Lets spend needless hours researching ' +
            'that) I hunkered down and absorbed the teachings of <a href="http://www.johnpapa.net/">John Papa</a>.</p>' +

            '<p>John Papa is one of those types that seems to have ingested a freight train\'s worth on information ' +
            '(you know... the really long one that has you waiting at the crossing for 10 minutes when you are in a rush ' +
            'to get your kid to hockey school) yet is still able to impart all of it in a nice even flow.</p>' +

            '<p>It worked wonders for me, and now I think I know everything.</p>' +

            '<p>OK, anyway, I\'m back aboard the Angular ferry again. I think, thanks to John Papa, I\'ve got this Gulp ' +
            'thing under control.</p>' +

            '<p>The next step is actual data services. If you are interested in looking at the code, check it out here... ' +
            '<a href="https://bitbucket.org/ghostmonk/ghostmonk-ui">ghostmonk-ui</a></p>'
        },

        'details-in-the-pipes' : {
            slug: 'details-in-the-pipes',
            title: 'Details in the Piping',
            publishDate: new Date(2015, 3, 30),
            preamble:'<p>Here\'s a crude, but fairly accurate metaphor: I\'ve been plumbing for ' +
            'the last couple of days.</p><p>When you yank off the cover and jam your arm, up to your ' +
            'elbow down the pipe, don\'t be suprised to find it covered in shit.</p>',
            complete: '<p>This is the point of no return, so there\'s no point in trying to clean up now. ' +
            'The only time for rags is when the job is complete... and well, I\'m still not complete.</p>' +
            '<p>For 5 days, almost every night, I\'ve been tinkering with NPM, Bower, Gulp and the many, many ' +
            'dependencies that make up the javascript build pipeline. It\'s mostly thankless work with little ' +
            'initial payoff, but I\'ve played with this sort of muck before, and I know the only option is to push ' +
            'on. These tools are alien to me, but the concepts are well understood. ' +
            'As as it all comes together, however, it is starting to feel pretty good. </p>'
        },

        'pull-myself-out-of-the-mud': {
            slug: 'pull-myself-out-of-the-mud',
            title: 'Pulling Myself Out of the Mud',
            publishDate: new Date(2015, 3, 24),
            preamble: '<p>I\'m taking a tiny detour from AngularJS tonight, and CSS Bootstrapping this site.</p>' +
            '<p>A colleague at work noticed that the site didn\'t load very well on his phone, so this update should fix all of that. ' +
            'While I\'m at it, I\'m going to dip my toes back into the CSS waters</p>',
            complete: '<p>It will be a short hiatus, but an important one none-the-less. As I said before, I really missed UI programming, ' +
            'CSS however, I loathed that with all of my being. Hopefully Bootstrap makes things a little easier.</p>' +
            '<p>Once I get images working, I might start taking screen shots of the progression.</p>'
        },

        'pimples-and-all': {
            slug: 'pimples-and-all',
            title: 'Pimples And All',
            publishDate: new Date(2015, 3, 23),
            preamble: '<p>Today I am becoming a services expert in Angular. It\'s a necessary progression.</p>' +
            '<p>I\'m in that horrible position were I don\'t really understand how little I know about the framework. ' +
            'I understand systems well enough to know that I am doing a number of things wrong, but I\'m still a little lost.</p>',
            complete:'<p>Right now I\'m dropping all of these stories, hard-coded, into a simple Controller with an array. It\'s very cumbersome, but fits with the philosophy' +
            'of getting things done even if I don\'t know how to get them done right.</p>' +
            '<p>I need to move all this data into something that more closely resembles a data service, so that I can swap out the array with a more appropriate data store.</p>' +
            '<p>My thoughts... which are subject to change, are that my next step, before wiring up the server, is to just place the data in multiple text files with names' +
            'that will resemble the parameters that I will use to query a service. I think I\'m missing something.</p>' +
            '<p>Are my posts confusing? I wouldn\'t worry about it too much. I am literally typing this post into string within a javascript object, that is withing an array, ' +
            'that is defined directly on a variable in an Angular Controller.</p>' +
            '<p>This is pure madness to be sure, but like this dirty aimless post, my main goal right now is to remove all barriers to getting things done.</p>' +
            '<p>Interestingly enough, that takes the form of two things: ' +
            '<ol>' +
            '<li>The quality of my writing.</li>' +
            '<li>The quality of my blog project</li>' +
            '</ol></p>' +
            '<p>If this idea of \'Do It Now, Worry About Results Later\' works out, things will start to get more interesting everyday.</p>'
        },

        'big-on-angular': {
            slug: 'big-on-angular',
            title: 'I\'m Big on Angular',
            publishDate: new Date(2015, 3, 22),
            preamble: '<p>AngularJS has me excited about Javascript. Don\'t get me wrong, I have always liked Javascript, but as ' +
            'more of a programming curiosity than something I would have devoted my time and energy to.</p>' +
            '<p>I am something of a codephile.. ',
            complete:'which I hope isn\'t an actual word... but what I mean is that I love programming languages. ' +
            'I love being exposed to different types of syntax, like List Comprehensions in Python, or the hyper conciseness of Haskell and the ingenious methods of LINQ. ' +
            'I also love different ideas, once devoting months of spare time writing LISP and Scheme for no other reason than to understand Functional Programming.</p>' +
            '<p>The great advantage of this is that I stop complaining about the silly thing that most programmers constantly gripe over... ' +
            'like C# isn\'t dynamic like PHP, or that Java is too verbose, or Python is too hard to read, braces should be on the same line... ' +
            'not on the next line... use an underscore for private variables... blah blah blah.</p>' +
            '<p>This sort of focus (of which I have to admit I was once guilty) really misses the point. Javascript fits this paradigm. ' +
            'It is generally unique because of the way that you express objects, its lack of classes, the mercurial nature of scopes and the ' +
            'power and danger of playing with the prototype. It drives some programmers crazy and others into a rapturous flow.</p>' +
            '<p>Javascript always remained a second hand citizen for me because I enjoy modeling hierarchies and found myself ' +
            'struggling whenever a project needed more than a couple of script files.</p>' +
            '<p>Then along comes AngularJS, and I realize just how wrong I was to think this way.</p>' +
            '<p>My favourite aspect of programming is solving problems through expressive use of syntax and structure in code. I never devoted enough time to ' +
            'see these sorts of solutions in Javascript. Good thing someone did.</p>' +
            '<p>So now I\'m writing Javascript again.. but under the beautifully imposed structure of AngularJS.</p>' +
            '<p>It makes working with javascript unnaturally joyful.</p>'
        },

        'programming-the-ui-again': {
            slug: 'programming-the-ui-again',
            title: 'Programing the UI once again',
            publishDate: new Date(2015, 3, 21),
            preamble: '<p>I\'m back in the saddle. So to speak.</p>' +
            '<p>Once upon a time, I was a 14-hour-a-day UI programmer. ' +
            'I spent so many nights drinking Monsters, slamming coffee, sleeping shallow and waking early, because I loved making things move.</p>',
            complete: '<p>When the designers were good, the result was beauty... when the designers were bad, I did what needed to be done, and moved on... fast.</p>' +
            '<p>Those were back in the days of ActionScript and flash, before my world was overwhelmed by IOS. Most of my kind moved to either Javascript or some flavour of mobile phone programming.</p> ' +
            '<p>I however, jumped into the .NET world and desktop programming. In fact, my last ActionScript project was more of ' +
            'a back end library than a real chewy UI freak fest.</p>' +
            '<p>Anyway, I seriously miss those days, as my real passion lied in making things move in beautiful ways. </p>' +
            '<p>This new site is ugly, but it\'s fun.</p>' +
            '<p>I\'m back in the saddle today, and I\'m not getting off any time soon.</p>'
        },

        'arrggggh': {
            slug: 'arrggggh',
            title: 'AAAAAAAAAAA RRRRRRRRRRR GGGGGGGGGGG',
            publishDate: new Date(2015, 3, 20),
            preamble: '<p>My old site was old and staganant and terrible....</p>' +
            '<p>I\'ve had enough... I\'m changing it.</p>',
            complete: ''
        }

    };

})();
