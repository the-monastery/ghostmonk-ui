'use strict';

//Not sure it's a good idea to place the app instance in the global space.
var ghostmonk = angular.module('ghostmonk', ['ngSanitize', 'ngResource', 'ngCookies', 'ngRoute']);

(function(){

    ghostmonk.config(ghostmonkConfig);
    ghostmonkConfig.$inject = ['$routeProvider', '$locationProvider'];

    function ghostmonkConfig($routeProvider, $locationProvider) {

        $routeProvider.when('/', {
            templateUrl:'templates/story-list.html',
            controller:'StoryListController'
        });

        $routeProvider.when('/story/:slug', {
            templateUrl:'../templates/full-story.html',
            controller:'StoryController'
        });

        $routeProvider.otherwise({redirectTo: '/'});

        $locationProvider.html5Mode(true);
    }

})();
