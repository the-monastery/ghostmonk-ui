/**
 * Created by nick on 2015-03-21.
 */
(function(){
    'use strict';

    ghostmonk.controller('StoryListController', StoryListController);
    StoryListController.$inject = ['$scope', '$location', 'storyDataService'];

    function StoryListController($scope, $location, storyDataService) {

        storyDataService.getStoryList()
          .then(storyListSuccess)
          .catch(storyListFail);

        function storyListSuccess(stories){
          $scope.stories = stories;
        }

        function storyListFail(reason){
          console.log(reason);
        }

        $scope.loadFullStory = function(slug){
            $location.url('/story/' + slug);
        };
    }
})();
