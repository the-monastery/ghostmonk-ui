/**
 * Created by nick on 2015-03-21.
 */
(function(){
    'use strict';

    ghostmonk.controller('StoryController', StoryController);
    StoryController.$inject = ['$scope', '$route', 'storyDataService'];

    function StoryController($scope, $route, storyDataService) {

        storyDataService
            .getStory($route.current.params.slug)
            .then(function(story){
                $scope.story = story;
            });

    }

})();
