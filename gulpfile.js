var gulp = require('gulp');
var runSequence = require('run-sequence');
var args = require('yargs').argv;
var del = require('del');
var browserSync = require('browser-sync');
var wiredep = require('wiredep');
var config = require('./gulp.config')();
var $ = require('gulp-load-plugins')({lazy:true});
var port = process.env.PORT || config.defaultPort;

gulp.task('default', ['help']);
gulp.task('help', $.taskListing);

gulp.task('build-release', function(onComplete){
    log('STARTING RELEASE BUILD');

    runSequence(
        'clean-release',
        'bump',
        ['images', 'fonts', 'release-templates', 'optimize'],
        'copy-release-files',
        'minimize-release-index',
        ['run-release-server', 'delete-template-cache'],
        onComplete);
});

gulp.task('minimize-release-index', function(){
    return gulp
        .src(config.release + 'index.html')
        .pipe($.minifyHtml({empty: true}))
        .pipe(gulp.dest(config.release));
});

gulp.task('delete-template-cache', function(onComplete){
    del(config.jsFolder + config.templateCache.file, onComplete);
});

gulp.task('run-release-server', function(){
    runServer(false);
});

gulp.task('clean-release', function (onComplete) {
    clean(config.release + '**/*.*', onComplete);
});

gulp.task('build-dev', ['copy-dev'], function(){
    log('STARTING DEV BUILD');
    runServer(true);
});

gulp.task('copy-dev', ['clean-dev', 'full-inject'], function(){
    return gulp
        .src(config.src + '**/*.*')
        .pipe(gulp.dest(config.dev));
});

gulp.task('clean-dev', function (onComplete) {
    clean(config.dev + '**/*.*', onComplete);
});

/****************************|
|  OPTIMIZATION              |
|****************************/
gulp.task('optimize', ['full-inject', 'template-cache'], function(){
    log('Optimizing the build');

    var assets = $.useref.assets({searchPath: config.src});
    var templateCache = config.jsFolder + config.templateCache.file;
    var cssFilter = $.filter('**/*.css');
    var jsLibFilter = $.filter('**/' + config.optimize.vendorJs);
    var jsAppFilter = $.filter('**/' + config.optimize.appJs);

    return gulp
        .src(config.index)
        .pipe($.plumber())
        .pipe($.inject( gulp.src(templateCache, {read: false}),
            {starttag: '<!-- inject:templates:js -->'}
        ))
        .pipe(assets)

        //CSS OPTIMIZATION
        .pipe(cssFilter)
        .pipe($.csso())
        .pipe(cssFilter.restore())

        //JS OPTIMIZATION
        .pipe(jsLibFilter)
        .pipe($.uglify())
        .pipe(jsLibFilter.restore())

        //ANGULAR ANNOTATION
        .pipe(jsAppFilter)
        .pipe($.ngAnnotate())
        .pipe($.uglify())
        .pipe(jsAppFilter.restore())

        //RESTORE INDEX WITH NEW VERSION
        .pipe($.rev())
        .pipe(assets.restore())
        .pipe($.revReplace())
        .pipe($.useref())
        .pipe(gulp.dest(config.release))

        //WRITE OUT CACHE BUSTING MANIFEST
        .pipe($.rev.manifest())
        .pipe(gulp.dest(config.release));
});

gulp.task('template-cache', function(){
    log('Creating Angular template cache.');

    return gulp
        .src(config.htmlTemplates)
        .pipe($.minifyHtml({empty: true}))
        .pipe($.angularTemplatecache(
            config.templateCache.file,
            config.templateCache.config
        ))
        .pipe(gulp.dest(config.jsFolder));
});

/****************************|
|  FILE INJECTION            |
|****************************/
gulp.task('full-inject', ['wire-deps', 'styles'], function(){
   return gulp
       .src(config.index)
       .pipe($.inject(gulp.src(config.css), config.injectOptions))
       .pipe(gulp.dest(config.src));
});

gulp.task('wire-deps', function(){
    return gulp
        .src(config.index)
        .pipe(wiredep.stream(config.bowerOptions))
        .pipe($.inject(gulp.src(config.js), config.injectOptions))
        .pipe(gulp.dest(config.src));
});

/****************************|
| STYLES                     |
|****************************/
gulp.task('styles', ['clean-styles'], function () {
    log('Running STYLES compilation etc.', 'blue');
    return gulp
        .src(config.less)
        .pipe($.plumber())
        .pipe($.less())
        //.on('error', logError)
        .pipe($.autoprefixer({browser:['last 3 version', '> %3']}))
        .pipe(gulp.dest(config.cssFolder))
});

gulp.task('clean-styles', function (onComplete) {
    clean(config.cssFolder + '**/*.css', onComplete);
});

/****************************|
|  COPY FILES                |
|****************************/
gulp.task('copy-release-files', function(){
    return gulp
        .src(config.releaseFiles)
        .pipe(gulp.dest(config.release));
});

/****************************|
|  FONTS                     |
|****************************/
gulp.task('fonts', ['clean-fonts'], function(){
    log('Copying Fonts', 'blue');

    return gulp
        .src(config.fonts)
        .pipe(gulp.dest(config.release + 'fonts'));
});

gulp.task('clean-fonts', function(onComplete){
    clean(config.release + 'fonts/**/*.*', onComplete);
});

/****************************|
|  TEMPLATES                 |
|****************************/
gulp.task('release-templates', ['clean-release-templates'], function(){
    log('Copying Templates to release;');

    return gulp
        .src(config.htmlTemplates)
        .pipe(gulp.dest(config.release + 'templates/'));
});

gulp.task('clean-release-templates', function(onComplete){
    clean(config.release + 'templates/**/*.*', onComplete);
});

/****************************|
|  IMAGES                    |
|****************************/
gulp.task('images', ['clean-images'], function(){
    log('Copying and Compressing Images');

    return gulp
        .src(config.images)
        .pipe($.imagemin({optimizationlevel:4}))
        .pipe(gulp.dest(config.release + 'imgs'));
});

gulp.task('clean-images', function(onComplete){
    clean(config.release + 'imgs/**/*.*', onComplete);
});

gulp.task('check-code', function () {
    log('Analyzing code style', 'blue');
    return gulp
        .src(config.alljs)
        .pipe($.if(args.verbose, $.print()))
        .pipe($.jscs())
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-stylish', {verbose:true}))
        .pipe($.jshint.reporter('fail'));
});

gulp.task('watch-styles', function () {
    gulp.watch([config.less], ['styles']);
});

gulp.task('full-clean', function(onComplete){
    log('FULL CLEAN');
    var toDelete = [].concat(config.build);
    log('Performing a FULL CLEAN');
    del(toDelete, onComplete);
});

gulp.task('clean-code', function(onComplete){
    var files = [].concat(
        config.build + '**/*.html',
        config.build + '**/*.js'
    );
    clean(files, onComplete);
});

/**
 * Bump the version:
 * --type=pre will bump the prerelease version *.*.*-x (default)
 * --type=patch will bump patch version *.*.x
 * --type=minor will bump the minor version *.x.*
 * --type=major will bump the major version x.*.*
 * --version=1.2.3 Will bump to a specific version
 */
gulp.task('bump', function(onComplete){
    var msg = 'Bumping versions';
    var type = args.type;
    var version = args.version;
    var options = {};
    if(version) {
        options.version = version;
        msg += ' to ' + version;
    } else {
        options.type = type || 'pre';
        msg += ' for a ' + options.type;
    }
    log(msg);
    gulp
        //FIRST BUMP THE ROOT PACKAGES
        .src(config.packages)
        .pipe($.print())
        .pipe($.bump(options))
        .pipe(gulp.dest(config.root))

        //THEN BUMP THE SRC PACKAGE
    gulp.src(config.serverVersionFile)
        .pipe($.print())
        .pipe($.bump(options))
        .pipe(gulp.dest(config.src));

    onComplete();
});

function changeEventHandler(event){
    var srcPattern = new RegExp('/.*(?=/' + config.source + ')/');
    log('File ' + event.path.replace(srcPattern, '') + ' ' + event.type);
}

function runServer(isDev){

    var nodeOptions = {
        script: config.nodeServer,
        delayTime: 1,
        env: {
            'PORT': port,
            'NODE_ENV': isDev ? 'dev' : 'release'
        },
        watch: [config.server]
    };

    $.nodemon(nodeOptions)
        .on('restart', function(ev){
            log('************* Server Restarting', 'blue');
            log('************* Files: \n' + ev, 'blue');
            setTimeout(function(){
                browserSync.notify('RESTARTING SERVER...');
                browserSync.reload({stream:false});
            }, config.browserReloadDelay);
        })
        .on('start', function(){
            log('************* Server Starting', 'blue');
            startBrowserSync(isDev);
        })
        .on('crash', function(){
            log('************* Server Crashed', 'red');
        })
        .on('exit', function(){
            log('************* Server Clean Exit', 'blue');
        });
}

function startBrowserSync(isDev) {
    if(args.nosync || browserSync.isActive){
        return;
    }

    log('Starting Browser Sync on  ' + port, 'blue');

    var conf = isDev ? {
        toWatch: [config.less],
        onChange: ['styles'],
        restart: [
            config.src + '**/*.*',
            '!' + config.less,
            config.cssFolder + '**/*.css']
    } : {
        toWatch: [
            config.less,
            config.js,
            config.html],
        onChange: ['optimize', browserSync.reload],
        restart: []};

    //This will invoke tasks when the watch files change
    gulp.watch(conf.toWatch, conf.onChange)
        .on('change', changeEventHandler);

    //This will simply restart the server whenever the listed
    // files (conf.restart) change
    var options = {
        proxy: 'localhost:' + port,
        port: 3000,
        files: conf.restart,
        ghostMode: {
            clicks: true,
            location: false,
            forms: true,
            scroll: true
        },
        injectChanges: true,
        logFileChanges: true,
        logLevel: 'debug',
        logPrefix: 'gulp-patterns',
        notify: true,
        reloadDelay: 3000
    };

    browserSync(options);
}

function clean(path, cb) {
    log('Cleaning ' + path, 'blue');
    del(path, cb);
}

function logError(error) {
    log('*** ERROR ***', 'red');
    log(error, 'red');
    log('*** END ***');
    this.emit('end');
}

function log(msg, color) {
    color = color ? color : 'blue';
    if (typeof(msg) === 'object') {
        for (var key in msg) {
            if (msg.hasOwnProperty(key)) {
                $.util.log($.util.colors[color](msg));
            }
        }
    }
    else {
        $.util.log($.util.colors[color](msg));
    }
}
