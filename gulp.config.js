module.exports = function () {

    var root = './';
    var appName = 'ghostmonk';
    var src = './src/';
    var build = './build/';
    var dev = build + 'dev/';
    var release = build + 'release/';
    var server = './server/';
    var cssFolder = src + 'css/';
    var jsFolder = src + 'js/';
    var templateBlob = 'templates/**/*.html';
    var serverConfigFile = src + 'web.config';
    var serverVersionFile = src + 'package.json';

    var applicationJs = 'app.js';
    var vendorJs = 'lib.js';

    return {

        /**************|
        |* FILE PATHS *|
        |**************/
        alljs: [
            src + 'js/**/*.js'
        ],
        root: root,
        less: cssFolder + 'main.less',
        css: cssFolder + 'main.css',
        cssFolder: cssFolder,
        jsFolder: jsFolder,
        js: src + 'js/**/*.js',
        src: src,
        releaseFiles: [serverConfigFile, serverVersionFile],
        index: src + 'index.html',
        fonts: src + 'bower_components/font-awesome/fonts/**/*.*',
        images: src + 'imgs/**/*.*',
        templateBlob: templateBlob,
        htmlTemplates: src + templateBlob,
        html: src + '**/*.html',
        build: build,
        dev: dev,
        release: release,

        optimize: {
            appJs: applicationJs,
            vendorJs: vendorJs
        },

        /**************************|
        |* TEMPLATE CACHE OPTIONS *|
        |**************************/
        templateCache: {
          file: 'template-cache.js',
          config: {
              module: appName,
              standAlone: false,
              root: 'templates/'
          }
        },

        /*****************|
        |* BOWER OPTIONS *|
        |*****************/
        bowerOptions: {
            bowerJs: require('./bower.json'),
            directory: src + 'bower_components/',
            ignorePath: '../..'
        },

        packages: ['./package.json', './bower.json'],
        serverVersionFile: serverVersionFile,

        /******************|
        |* INJECT OPTIONS *|
        |******************/
        injectOptions: {
            ignorePath:'src',
            addRootSlash:false
        },

        /****************|
        |* NODE OPTIONS *|
        |****************/
        defaultPort: 8585,
        nodeServer: server + 'web-server.js',
        server: server,
        browserReloadDelay: 3000
    };
};
