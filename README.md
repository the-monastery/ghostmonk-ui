# ghostmonk-ui — A client side weblog

This application is the personal website of Nicholas Hillier, aka Ghostmonk.
It is a basic blog, but lends heavily from AngularJS and Angular Seed as well as other popular Javascript libraries.
This ReadMe, for instance, is essentially the Angular Seed ReadMe re-purposed.
This project was started by completely mangling an angular-seed clone.

## Getting Started

To get you started just clone ghostmonk-ui repository and install the dependencies:

### Prerequisites

GIT
[http://git-scm.com/](http://git-scm.com/).

NODE and NPM
[http://nodejs.org/](http://nodejs.org/).

### Clone ghostmonk-ui

Use [git][git]:

```
git clone https://github.com/ghostmonk/ghostmonk-ui.git
cd ghostmonk-ui
```

To start a ghostmonk-ui project without the history

```bash
git clone --depth=1 https://github.com/ghostmonk/ghostmonk-ui.git <your-project-name>
```

### Install Dependencies

There are two kinds of dependencies in this project - tools, and client side code

* We get tools using `npm`, the [node package manager][npm].
* We get client bower `bower`, a [client-side code package manager][bower].

npm install will call bower install, so all you need to do is the following.

```
npm install
```

This will create two new folders with the necessary dependencies as described above.

* `node_modules` - tools
* `src/bower_components` - client code

*Note the ./src/ folder is the root of our application. This is where the index.html file is found.

### Run the Application

This project comes with a fairly standard node configured server that is STRICTLY meant for development purposes.
This script is unsafe to use in a production environment as it allows users to write to your server.

Open a terminal or CMD from the root of ghostmonk-ui and do the following

```
cd src
node ../web-server.js
```

You will now be able to load ghostmonk-ui in a browser at the following location. http://localhost:8000.
*Note It is possible that port 8000 is already in use on your machine. If so, the above location may not work reliably.


## Directory Layout

```
src/                    --> all of the source files for the application
  css/
    app.css               --> default stylesheet
  js/                     --> the main folder for custom application javascript
    controllers/          --> custom angular controllers
    directives/           --> custom angular directives
    filters/              --> custom angular filters
    services/             --> custom angular services
    main.js                --> main application module
    templates/            --> snippets and views loaded into ng-view in index.html
    index.html            --> app layout file (the main html template file of the app)
```