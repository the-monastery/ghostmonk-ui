module.exports = function(){
    var service = {
        getJsonFromFile: getJsonFromFile
    };
    return service;

    function getJsonFromFile(file) {
        var fs = require('fs');
        var filePath = __dirname + file;
        return JSON.parse(fs.readFileSync(filePath, 'utf8'));
    }
}
