module.exports = function(app){
    var api = '/api';
    var data = '/data/';
    var jsonFileService = require('./jsonFileService')();

    app.get( app + '/story/:slug', getStory );
    app.get( app + '/stories', getStories );

    function getStory(request, response, next) {
        var json = jsonFileService.getJsonFromFile( data + 'stories.json' );
        var story = json.filter( function(s) {
            return s.slug === request.params.slug;
        });
        response.send(story[0]);
    }

    function getStories(request, response, next) {
        response.send(
            jsonFileService.getJsonFromFile( data + 'stories.json' )
        );
    }
}
